import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DataModule } from './data/data.module';
import { CoreModule } from './core/core.module';
import { DogRepository } from './core/repositories/dog.repository';
import { PresentationModule } from './presentation/presentation.module';
import { DogMockRepository } from './data/repository/dog-mock-repository/dog-mock.repository';
import { DogWebRepository } from './data/repository/dog-web-repository/dog-web.repository';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DataModule,
    CoreModule,
    PresentationModule
  ],
  providers: [
    {provide: DogRepository, useClass: DogWebRepository}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
