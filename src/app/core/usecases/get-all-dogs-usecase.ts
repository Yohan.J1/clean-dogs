import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UseCase } from '../base/use-case';
import { DogModel } from '../domain/dog.model';
import { DogRepository } from '../repositories/dog.repository';

@Injectable({
	providedIn: 'root'
})
export class GetAllDogsUsecase implements UseCase<void, DogModel> {

	constructor(private dogRepository: DogRepository) { }

	execute(){
		return this.dogRepository.getAll();
	}
}
