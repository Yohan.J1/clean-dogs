import { Observable } from 'rxjs';
import { DogModel } from '../domain/dog.model';

export abstract class DogRepository {
	abstract getById(name: string): Observable<DogModel>;
	abstract getAll(): Observable<DogModel>;
}
