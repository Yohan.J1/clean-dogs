import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { flatMap, map } from "rxjs/operators";
import { DogRepository } from "../../../core/repositories/dog.repository";
import { DogModel } from "../../../core/domain/dog.model";
import { DogWebRepositoryMapper } from "./dog-web-repository-mapper";
import { DogWebEntity } from "./dog-web-entity";

@Injectable({
  providedIn: 'root'
})

export class DogWebRepository extends DogRepository {

  mapper = new DogWebRepositoryMapper();
  url: string = 'http://5b8d40db7366ab0014a29bfa.mockapi.io/api/v1/elephants';

  constructor( private http: HttpClient) {
    super();
  }

  getById(id: string): Observable<DogModel>{
    return this.http
      .get<DogWebEntity>(`${this.url}/${id}`)
      .pipe(map(this.mapper.mapFrom))
  }

  getAll(): Observable<DogModel>{
    return this.http
      .get<DogWebEntity[]>(`${this.url}`)
      .pipe(flatMap((item) => item))
      .pipe(map(this.mapper.mapFrom))
  }
}
