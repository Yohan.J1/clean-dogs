import { DogModel } from '../../../core/domain/dog.model';
import { Mapper } from '../../../core/base/mapper';
import { DogWebEntity } from './dog-web-entity';

export class DogWebRepositoryMapper extends Mapper <DogWebEntity, DogModel> {
  mapFrom(param: DogWebEntity): DogModel {
    return {
      name: param.name
    };
  }

  mapTo(param: DogModel): DogWebEntity {
    return {
      name: param.name
    };
  }
}