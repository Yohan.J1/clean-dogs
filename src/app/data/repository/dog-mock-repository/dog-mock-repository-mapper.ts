import { DogModel } from '../../../core/domain/dog.model';
import { Mapper } from '../../../core/base/mapper';
import { DogMockEntity } from './dog-mock-entity';

export class DogMockRepositoryMapper extends Mapper <DogMockEntity, DogModel> {
  mapFrom(param: any): DogModel {
    return {
      name: param.name
    };
  }

  mapTo(param: DogModel): DogMockEntity {
    return {
      name: param.name
    };
  }
}
