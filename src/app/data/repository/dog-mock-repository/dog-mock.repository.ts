import { Injectable } from '@angular/core';
import { DogRepository } from '../../../core/repositories/dog.repository';
import { DogModel } from '../../../core/domain/dog.model';
import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DogMockRepositoryMapper } from './dog-mock-repository-mapper';
import { DogMockEntity } from './dog-mock-entity';
import { dogs } from './data-mok-dog'

@Injectable({
  providedIn: 'root'
})
export class DogMockRepository extends DogRepository {

  private mapper = new DogMockRepositoryMapper();

  constructor() { super() }

  getById(name: string): Observable<DogModel> {
    return from(dogs)
      .pipe(filter((dog: DogMockEntity) => dog.name === name))
      .pipe(map(this.mapper.mapFrom));
  }

  getAll(): Observable<DogModel>{
    return from(dogs).pipe(map(this.mapper.mapFrom))
  }
}