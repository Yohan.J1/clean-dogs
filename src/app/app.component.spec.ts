import { async, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { DogRepository } from './core/repositories/dog.repository';
import { DogMockRepository } from './data/repository/dog-mock-repository/dog-mock.repository';
import { DogCardListComponent } from './presentation/dog-card-list/dog-card-list.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        DogCardListComponent
      ],
      providers: [
        {provide: DogRepository, useClass: DogMockRepository}
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
