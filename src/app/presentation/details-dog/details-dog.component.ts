import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-details-dog',
  templateUrl: './details-dog.component.html',
  styleUrls: ['./details-dog.component.scss']
})
export class DetailsDogComponent implements OnInit {

  dataBreed: Array<any> = [];
  public nameBreed: String;

  constructor(private route: ActivatedRoute) {
    this.nameBreed = this.route.snapshot.params.breed
  }

  ngOnInit() {
  }

}
