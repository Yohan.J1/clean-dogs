import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DogCardListComponent } from './dog-card-list/dog-card-list.component';
import { CoreModule } from '../core/core.module';
import { DataModule } from '../data/data.module';
import { DetailsDogComponent } from './details-dog/details-dog.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    DataModule
  ],
  declarations: [
    DogCardListComponent,
    DetailsDogComponent
  ],
  exports: [
    DogCardListComponent
  ],
  providers: [
  ]
})
export class PresentationModule { }
