import { Component, OnInit } from '@angular/core';
import { DogModel } from "../../core/domain/dog.model";
import { GetAllDogsUsecase } from "../../core/usecases/get-all-dogs-usecase";

@Component({
	selector: 'app-dog-card',
	templateUrl: './dog-card-list.component.html',
	styleUrls: []
})

export class DogCardListComponent implements OnInit{

	dogs: Array<DogModel>;
	
	constructor(
		private getAllDogs: GetAllDogsUsecase
		) {
			this.dogs = [];
	}

	ngOnInit(){
		this.getDogs();
	}

	getDogs(){
		this.getAllDogs.execute().subscribe((value: DogModel) =>{
			this.dogs.push(value)
			console.log('HEEEERE', value)
		})
	}
}
